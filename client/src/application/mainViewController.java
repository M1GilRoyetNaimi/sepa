package application;


import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import javafx.fxml.FXML;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class mainViewController {
	
	@FXML
	private TabPane tabPane;

	@FXML
	private TextArea statsArea;
	
	@FXML
	private TextArea resumeArea;
	
	@FXML
	private TextArea trxArea;
	
	@FXML
	private TextArea depotBodyArea;
	
	@FXML
	private TextArea depotResultArea;
	
	@FXML
	private TextField idTrxField;
	
	private Service service;
    

    private static final QName qname = new QName("", "");
    private static final String url = "https://pure-journey-55804.herokuapp.com";
	
	@FXML
	private void doLaunch() {
		switch (tabPane.getSelectionModel().getSelectedItem().getText()) {
			case "stats" :
				getResultStats();
				break;
			case "resume" :
				getResultResume();
				break;
			case "trx" :
				getResultTrx();
				break;
			case "depot" :
				getResultDepot();
				break;
		}
	}

	public String request(String urlParam, String httpMethod, String body) {
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + urlParam);
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("Content-Type", Arrays.asList(new String[] {"application/xml"}));
        headers.put("Accept", Arrays.asList(new String[] {"application/xml"}));
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
 
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, httpMethod);
        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        Source source = null;
	        if (body.equals("")) {
	        	source = dispatcher.invoke(new StreamSource(new StringReader("<?xml version=\"1.0\"?><root>aaaa</root>")));
	        } else {
	        	source = dispatcher.invoke(new StreamSource(new StringReader(body)));
	        }

           /* Integer statusCode = (Integer) dispatcher.getResponseContext().get(MessageContext.HTTP_RESPONSE_CODE);
            if (statusCode != 200 && statusCode != 201) {
            	source = new StreamSource(new StringReader("<?xml version=\"1.0\"?><root>"
            			+ "Error : " + statusCode 
            			+ "</root>"));
            }*/
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer;
        String strResult = "";
		try {
			transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(source,result);
	        strResult = writer.toString().replaceFirst(">", ">\n");
	        
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return strResult;
    }
	

	private void getResultDepot() {
		depotResultArea.setText(request("/depot","POST", depotBodyArea.getText()));
		
	}

	private void getResultTrx() {
		trxArea.setText(request("/trx/" + idTrxField.getText().replaceAll(" ", "%20"),"GET", ""));
		
	}

	private void getResultResume() {
		resumeArea.setText(request("/resume","GET", ""));
		
	}

	private void getResultStats() {
		statsArea.setText(request("/stats","GET", ""));
		
	}
	
}
