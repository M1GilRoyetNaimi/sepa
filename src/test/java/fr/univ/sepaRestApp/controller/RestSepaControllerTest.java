package fr.univ.sepaRestApp.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import urouen.sepa.configuration.AppConfig;
import urouen.sepa.model.DrctDbtTxInf;
import urouen.sepa.model.Stats;
import urouen.sepa.repository.DrctDbtTxinfRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = AppConfig.class)
public class RestSepaControllerTest {
	
	public final static String SAMPLE_ROOT = "/sample";
	public final static String STATS_ROOT = "/stats";
	public final static String RESUME_ROOT = "/resume";
	public final static String TRX_N_ROOT = "/trx/";
	public final static String DEPOT_ROOT = "/depot";
	
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	
	@Autowired
	private DrctDbtTxinfRepository drctDbtTxinfRepository;
	
	
	private static boolean isInitialized = false;
	@Before
	public void runOnce() {
		if (isInitialized) return;
		
		//GIVEN
		DrctDbtTxInf sample1 = this.restTemplate.getForObject(SAMPLE_ROOT, DrctDbtTxInf.class);
		sample1.setPmtId("1");
		sample1.getInstdAmt().setValue(new BigDecimal(10));
		DrctDbtTxInf sample2 = this.restTemplate.getForObject(SAMPLE_ROOT, DrctDbtTxInf.class);
		sample2.setPmtId("2");
		sample2.getInstdAmt().setValue(new BigDecimal(5));
		
		DrctDbtTxInf sample3 = this.restTemplate.getForObject(SAMPLE_ROOT, DrctDbtTxInf.class);
		sample3.setPmtId("3");
		sample2.getInstdAmt().setValue(new BigDecimal(2));
		
		
		//drctDbtTxinfRepository.save(Arrays.asList(sample1, sample2, sample3));
		drctDbtTxinfRepository.save(sample1);
		drctDbtTxinfRepository.save(sample2);
		drctDbtTxinfRepository.save(sample3);
		
		
		isInitialized = true;
	}
	 
	@Test
	public void exampleTest() throws Exception {
		DrctDbtTxInf sample = this.restTemplate.getForObject(SAMPLE_ROOT, DrctDbtTxInf.class);
		
		assertThat(sample.getDbtr()).isNotNull();
	}
	
	
	
	@Test
	public void statsTest() throws Exception {
		//GIVEN
		Iterable<DrctDbtTxInf> transactions = drctDbtTxinfRepository.findAll();
		
		int count = 0;
		double total = 0;
		 
		for (DrctDbtTxInf transac : transactions) {
			count++;
			total += transac.getInstdAmt().getValue().doubleValue();
		}
		
		//WHEN
		ResponseEntity<Stats> request = this.restTemplate.getForEntity(STATS_ROOT, Stats.class);
		Stats stats = request.getBody();
		
		//THEN
		assertThat(request.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(stats.getNbTrx()).isEqualTo(count);
		assertThat(stats.getTotalAmountTrx()).isEqualTo(total);
	}
	
	@Test
	public void getTransactionTestSuccess() throws Exception {
		//GIVEN
		String pmtId = "2";
		DrctDbtTxInf expectedTrx = drctDbtTxinfRepository.findByPmtId(pmtId);
		
		//WHEN
		ResponseEntity<DrctDbtTxInf> request = this.restTemplate.getForEntity(TRX_N_ROOT + pmtId, DrctDbtTxInf.class);
		DrctDbtTxInf body = request.getBody();
		
		//THEN
		assertThat(request.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(body.getPmtId()).isEqualTo(expectedTrx.getPmtId());
		assertThat(body.getInstdAmt().getValue()).isEqualTo(expectedTrx.getInstdAmt().getValue());
		
	}
	
	@Test
	public void getTransactionTestNotFound() throws Exception {
		//WHEN
		ResponseEntity<?> request = this.restTemplate.getForEntity(TRX_N_ROOT + 100, Object.class);
		
		//THEN
		assertThat(request.getStatusCodeValue()).isEqualTo(404);
	}
	
	@Test
	public void depotTestSuccess() throws Exception {
		DrctDbtTxInf sample = restTemplate.getForObject(SAMPLE_ROOT, DrctDbtTxInf.class);
		String pmtId = "5";
		sample.setPmtId(pmtId);
		sample.getInstdAmt().setValue(new BigDecimal(100));
		
		//WHEN
		ResponseEntity<String> request = this.restTemplate
				.postForEntity(DEPOT_ROOT, sample, String.class);
		System.out.println(request.getBody());
		//THEN
		assertThat(request.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertTrue(request.getBody().split("<String>")[1].split("</")[0].matches("NR[0-9]{4}"));
		DrctDbtTxInf persistedTrx = drctDbtTxinfRepository.findByPmtId(pmtId);
		assertThat(persistedTrx.getPmtId()).isEqualTo(pmtId);
		assertThat(persistedTrx.getInstdAmt().getValue().doubleValue())
				.isEqualTo(sample.getInstdAmt().getValue().doubleValue());
	
	}
	
	
	
	
}
