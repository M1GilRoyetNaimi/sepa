package urouen.sepa.utils;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import urouen.sepa.repository.DrctDbtTxinfRepository;

@Component
public class NumGenerator implements IdentifierGenerator {

	
	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		String prefix = "NR";
		
		ApplicationContext ctx =  ApplicationContextUtils.getApplicationContext();
		DrctDbtTxinfRepository drctDbtTxinfRepository =ctx
				.getBean(DrctDbtTxinfRepository.class);
		return prefix 
				+ String.format("%04d",(drctDbtTxinfRepository
						.count() + 1));
	}

}
