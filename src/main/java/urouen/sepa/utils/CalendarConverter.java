package urouen.sepa.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;


@Converter
public class CalendarConverter implements AttributeConverter<XMLGregorianCalendar, Calendar> {

	@Override
	public Calendar convertToDatabaseColumn(XMLGregorianCalendar arg0) {
		if (arg0 == null) return null; 
		Calendar cal = arg0.toGregorianCalendar();
		return cal;
	}

	@Override
	public XMLGregorianCalendar convertToEntityAttribute(Calendar arg0) {
		XMLGregorianCalendar cal = null;
		if (arg0 instanceof GregorianCalendar) {
			try {
				cal = DatatypeFactory.newInstance().newXMLGregorianCalendar((GregorianCalendar) arg0);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}

		
		return cal;
	}

}
