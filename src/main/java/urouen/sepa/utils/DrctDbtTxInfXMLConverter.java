package urouen.sepa.utils;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;

import urouen.sepa.model.DrctDbtTxInf;

public class DrctDbtTxInfXMLConverter implements HttpMessageConverter<DrctDbtTxInf> {

	protected MarshallingHttpMessageConverter delegateConverter;
	
	public DrctDbtTxInfXMLConverter(MarshallingHttpMessageConverter converter) {
		this.delegateConverter = converter;
	}
	
	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return DrctDbtTxInf.class.equals(clazz) && delegateConverter.canRead(clazz, mediaType);
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return DrctDbtTxInf.class.equals(clazz) && delegateConverter.canWrite(clazz, mediaType);
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return delegateConverter.getSupportedMediaTypes();
	}

	@Override
	public DrctDbtTxInf read(Class<? extends DrctDbtTxInf> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		
		DrctDbtTxInf result = (DrctDbtTxInf) delegateConverter.read(clazz, inputMessage);
       
		return result;
	}

	@Override
	public void write(DrctDbtTxInf t, MediaType contentType, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		
		delegateConverter.write(t, contentType, outputMessage);
	}

}
