//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.04.03 à 05:41:29 PM CEST 
//


package urouen.sepa.model;

import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import urouen.sepa.utils.CalendarConverter;


/**
 * <p>Classe Java pour CmpsdTx complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CmpsdTx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MndtRltdInf">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MndtId" type="{http://univ.fr/sepa}Max35Text"/>
 *                   &lt;element name="DtOfSgntr" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CmpsdTx", propOrder = {
    "mndtRltdInf"
})
@Embeddable
public class CmpsdTx {

    @XmlElement(name = "MndtRltdInf", required = true)
    protected CmpsdTx.MndtRltdInf mndtRltdInf;

    /**
     * Obtient la valeur de la propriété mndtRltdInf.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdTx.MndtRltdInf }
     *     
     */
    public CmpsdTx.MndtRltdInf getMndtRltdInf() {
        return mndtRltdInf;
    }

    /**
     * Définit la valeur de la propriété mndtRltdInf.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdTx.MndtRltdInf }
     *     
     */
    public void setMndtRltdInf(CmpsdTx.MndtRltdInf value) {
        this.mndtRltdInf = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MndtId" type="{http://univ.fr/sepa}Max35Text"/>
     *         &lt;element name="DtOfSgntr" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mndtId",
        "dtOfSgntr"
    })
    @Embeddable
    public static class MndtRltdInf {

        @XmlElement(name = "MndtId", required = true)
        protected String mndtId;
        @XmlElement(name = "DtOfSgntr", required = true)
        @XmlSchemaType(name = "date")
        @Convert(converter=CalendarConverter.class)
        protected XMLGregorianCalendar dtOfSgntr;

        /**
         * Obtient la valeur de la propriété mndtId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMndtId() {
            return mndtId;
        }

        /**
         * Définit la valeur de la propriété mndtId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMndtId(String value) {
            this.mndtId = value;
        }

        /**
         * Obtient la valeur de la propriété dtOfSgntr.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDtOfSgntr() {
            return dtOfSgntr;
        }

        /**
         * Définit la valeur de la propriété dtOfSgntr.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDtOfSgntr(XMLGregorianCalendar value) {
            this.dtOfSgntr = value;
        }

    }

}
