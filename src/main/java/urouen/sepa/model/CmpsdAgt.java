//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.04.03 à 05:41:29 PM CEST 
//


package urouen.sepa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour CmpsdAgt complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CmpsdAgt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinInstnId">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="BIC" type="{http://univ.fr/sepa}BICType"/>
 *                   &lt;element name="Othr">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Id" type="{http://univ.fr/sepa}Max35Text"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CmpsdAgt", propOrder = {
    "finInstnId"
})
@Embeddable
public class CmpsdAgt {

    @XmlElement(name = "FinInstnId", required = true)
    protected CmpsdAgt.FinInstnId finInstnId;

    /**
     * Obtient la valeur de la propriété finInstnId.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdAgt.FinInstnId }
     *     
     */
    public CmpsdAgt.FinInstnId getFinInstnId() {
        return finInstnId;
    }

    /**
     * Définit la valeur de la propriété finInstnId.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdAgt.FinInstnId }
     *     
     */
    public void setFinInstnId(CmpsdAgt.FinInstnId value) {
        this.finInstnId = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="BIC" type="{http://univ.fr/sepa}BICType"/>
     *         &lt;element name="Othr">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Id" type="{http://univ.fr/sepa}Max35Text"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bic",
        "othr"
    })
    @Embeddable
    public static class FinInstnId {

        @XmlElement(name = "BIC")
        protected String bic;
        @XmlElement(name = "Othr")
        protected CmpsdAgt.FinInstnId.Othr othr;

        /**
         * Obtient la valeur de la propriété bic.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBIC() {
            return bic;
        }

        /**
         * Définit la valeur de la propriété bic.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBIC(String value) {
            this.bic = value;
        }

        /**
         * Obtient la valeur de la propriété othr.
         * 
         * @return
         *     possible object is
         *     {@link CmpsdAgt.FinInstnId.Othr }
         *     
         */
        public CmpsdAgt.FinInstnId.Othr getOthr() {
            return othr;
        }

        /**
         * Définit la valeur de la propriété othr.
         * 
         * @param value
         *     allowed object is
         *     {@link CmpsdAgt.FinInstnId.Othr }
         *     
         */
        public void setOthr(CmpsdAgt.FinInstnId.Othr value) {
            this.othr = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Id" type="{http://univ.fr/sepa}Max35Text"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id"
        })
        @Embeddable
        public static class Othr {
        	
        	@Column(name="othrId")
            @XmlElement(name = "Id", required = true)
            protected String id;

            /**
             * Obtient la valeur de la propriété id.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Définit la valeur de la propriété id.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

        }

    }

}
