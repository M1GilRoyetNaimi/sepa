package urouen.sepa.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Stats {
	
	
	@XmlElement
	protected int nbTrx;
	
	@XmlElement
	protected double totalAmountTrx;
	
	public Stats(int nbTrx, double total) {
		super();
		this.nbTrx = nbTrx;
		this.totalAmountTrx = total;
	}
	
	public Stats() {
	}
	
	public int getNbTrx() {
		return nbTrx;
	}

	public void setNbTrx(int nbTrx) {
		this.nbTrx = nbTrx;
	}

	public double getTotalAmountTrx() {
		return totalAmountTrx;
	}

	public void setTotalAmountTrx(double totalAmountTrx) {
		this.totalAmountTrx = totalAmountTrx;
	}

	@Override
	public String toString() {
		return "Stats [nbTrx=" + nbTrx + ", totalAmountTrx=" + totalAmountTrx + "]";
	}

	
	
}
