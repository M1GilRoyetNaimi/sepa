//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.04.03 à 05:41:29 PM CEST 
//


package urouen.sepa.model;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour CmpsdTpInf complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CmpsdTpInf">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SvcLvl">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Cd">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="SEPA"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LclInstrm">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Cd">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="CORE"/>
 *                         &lt;enumeration value="B2B"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SeqTp">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="OOFF"/>
 *               &lt;enumeration value="RCUR"/>
 *               &lt;enumeration value="FRST"/>
 *               &lt;enumeration value="FNAL"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CmpsdTpInf", propOrder = {
    "svcLvl",
    "lclInstrm",
    "seqTp"
})
@Embeddable
public class CmpsdTpInf {

    @XmlElement(name = "SvcLvl", required = true)
    protected CmpsdTpInf.SvcLvl svcLvl;
    @XmlElement(name = "LclInstrm", required = true)
    protected CmpsdTpInf.LclInstrm lclInstrm;
    @XmlElement(name = "SeqTp", required = true)
    protected String seqTp;

    /**
     * Obtient la valeur de la propriété svcLvl.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdTpInf.SvcLvl }
     *     
     */
    public CmpsdTpInf.SvcLvl getSvcLvl() {
        return svcLvl;
    }

    /**
     * Définit la valeur de la propriété svcLvl.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdTpInf.SvcLvl }
     *     
     */
    public void setSvcLvl(CmpsdTpInf.SvcLvl value) {
        this.svcLvl = value;
    }

    /**
     * Obtient la valeur de la propriété lclInstrm.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdTpInf.LclInstrm }
     *     
     */
    public CmpsdTpInf.LclInstrm getLclInstrm() {
        return lclInstrm;
    }

    /**
     * Définit la valeur de la propriété lclInstrm.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdTpInf.LclInstrm }
     *     
     */
    public void setLclInstrm(CmpsdTpInf.LclInstrm value) {
        this.lclInstrm = value;
    }

    /**
     * Obtient la valeur de la propriété seqTp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeqTp() {
        return seqTp;
    }

    /**
     * Définit la valeur de la propriété seqTp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeqTp(String value) {
        this.seqTp = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Cd">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="CORE"/>
     *               &lt;enumeration value="B2B"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cd"
    })
    public static class LclInstrm {

        @XmlElement(name = "Cd", required = true)
        protected String cd;

        /**
         * Obtient la valeur de la propriété cd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCd() {
            return cd;
        }

        /**
         * Définit la valeur de la propriété cd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCd(String value) {
            this.cd = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Cd">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="SEPA"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cd"
    })
    public static class SvcLvl {

        @XmlElement(name = "Cd", required = true)
        protected String cd;

        /**
         * Obtient la valeur de la propriété cd.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCd() {
            return cd;
        }

        /**
         * Définit la valeur de la propriété cd.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCd(String value) {
            this.cd = value;
        }

    }

}
