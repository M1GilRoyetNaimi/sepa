//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.04.03 à 05:41:29 PM CEST 
//


package urouen.sepa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.GenericGenerator;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PmtId" type="{http://univ.fr/sepa}Max35Text"/>
 *         &lt;element name="InstdAmt" type="{http://univ.fr/sepa}Amount"/>
 *         &lt;element name="DrctDbtTx" type="{http://univ.fr/sepa}CmpsdTx"/>
 *         &lt;element name="DbtrAgt" type="{http://univ.fr/sepa}CmpsdAgt"/>
 *         &lt;element name="Dbtr" type="{http://univ.fr/sepa}CmpsdNmd"/>
 *         &lt;element name="DbtrAcct" type="{http://univ.fr/sepa}CmpsdIdt"/>
 *         &lt;element name="RmtInf" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pmtId",
    "instdAmt",
    "drctDbtTx",
    "dbtrAgt",
    "dbtr",
    "dbtrAcct",
    "rmtInf"
})
@XmlRootElement(name = "DrctDbtTxInf")
@Entity
public class DrctDbtTxInf {

    @XmlElement(name = "PmtId", required = true)
    protected String pmtId;
    
    @XmlTransient
    @GeneratedValue(generator="numGen")
    @GenericGenerator(name="numGen", strategy="urouen.sepa.utils.NumGenerator")
    @Id
    protected String num;
    
    @XmlElement(name = "InstdAmt", required = true)
    protected Amount instdAmt;
    @XmlElement(name = "DrctDbtTx", required = true)
    protected CmpsdTx drctDbtTx;
    @XmlElement(name = "DbtrAgt", required = true)
    protected CmpsdAgt dbtrAgt;
    @XmlElement(name = "Dbtr", required = true)
    protected CmpsdNmd dbtr;
    @XmlElement(name = "DbtrAcct", required = true)
    protected CmpsdIdt dbtrAcct;
    @XmlElement(name = "RmtInf")
    
    @ElementCollection
    protected List<String> rmtInf;

    /**
     * Obtient la valeur de la propriété pmtId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmtId() {
        return pmtId;
    }

    /**
     * Définit la valeur de la propriété pmtId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmtId(String value) {
        this.pmtId = value;
    }
    
    /**
     * Obtient la valeur de la propriété num.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Définit la valeur de la propriété num.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

    /**
     * Obtient la valeur de la propriété instdAmt.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getInstdAmt() {
        return instdAmt;
    }

    /**
     * Définit la valeur de la propriété instdAmt.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setInstdAmt(Amount value) {
        this.instdAmt = value;
    }

    /**
     * Obtient la valeur de la propriété drctDbtTx.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdTx }
     *     
     */
    public CmpsdTx getDrctDbtTx() {
        return drctDbtTx;
    }

    /**
     * Définit la valeur de la propriété drctDbtTx.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdTx }
     *     
     */
    public void setDrctDbtTx(CmpsdTx value) {
        this.drctDbtTx = value;
    }

    /**
     * Obtient la valeur de la propriété dbtrAgt.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdAgt }
     *     
     */
    public CmpsdAgt getDbtrAgt() {
        return dbtrAgt;
    }

    /**
     * Définit la valeur de la propriété dbtrAgt.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdAgt }
     *     
     */
    public void setDbtrAgt(CmpsdAgt value) {
        this.dbtrAgt = value;
    }

    /**
     * Obtient la valeur de la propriété dbtr.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdNmd }
     *     
     */
    public CmpsdNmd getDbtr() {
        return dbtr;
    }

    /**
     * Définit la valeur de la propriété dbtr.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdNmd }
     *     
     */
    public void setDbtr(CmpsdNmd value) {
        this.dbtr = value;
    }

    /**
     * Obtient la valeur de la propriété dbtrAcct.
     * 
     * @return
     *     possible object is
     *     {@link CmpsdIdt }
     *     
     */
    public CmpsdIdt getDbtrAcct() {
        return dbtrAcct;
    }

    /**
     * Définit la valeur de la propriété dbtrAcct.
     * 
     * @param value
     *     allowed object is
     *     {@link CmpsdIdt }
     *     
     */
    public void setDbtrAcct(CmpsdIdt value) {
        this.dbtrAcct = value;
    }

    /**
     * Gets the value of the rmtInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rmtInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRmtInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRmtInf() {
        if (rmtInf == null) {
            rmtInf = new ArrayList<String>();
        }
        return this.rmtInf;
    }
    
}
