//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2017.04.03 à 05:41:29 PM CEST 
//


package urouen.sepa.model;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.univ.sepa package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.univ.sepa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CmpsdTpInf }
     * 
     */
    public CmpsdTpInf createCmpsdTpInf() {
        return new CmpsdTpInf();
    }

    /**
     * Create an instance of {@link CmpsdIdt }
     * 
     */
    public CmpsdIdt createCmpsdIdt() {
        return new CmpsdIdt();
    }

    /**
     * Create an instance of {@link CmpsdIdt.Id }
     * 
     */
    public CmpsdIdt.Id createCmpsdIdtId() {
        return new CmpsdIdt.Id();
    }

    /**
     * Create an instance of {@link CmpsdIdt.Id.PrvtId }
     * 
     */
    public CmpsdIdt.Id.PrvtId createCmpsdIdtIdPrvtId() {
        return new CmpsdIdt.Id.PrvtId();
    }

    /**
     * Create an instance of {@link CmpsdIdt.Id.PrvtId.Othr }
     * 
     */
    public CmpsdIdt.Id.PrvtId.Othr createCmpsdIdtIdPrvtIdOthr() {
        return new CmpsdIdt.Id.PrvtId.Othr();
    }

    /**
     * Create an instance of {@link CmpsdAgt }
     * 
     */
    public CmpsdAgt createCmpsdAgt() {
        return new CmpsdAgt();
    }

    /**
     * Create an instance of {@link CmpsdAgt.FinInstnId }
     * 
     */
    public CmpsdAgt.FinInstnId createCmpsdAgtFinInstnId() {
        return new CmpsdAgt.FinInstnId();
    }

    /**
     * Create an instance of {@link CmpsdTx }
     * 
     */
    public CmpsdTx createCmpsdTx() {
        return new CmpsdTx();
    }

    /**
     * Create an instance of {@link DrctDbtTxInf }
     * 
     */
    public DrctDbtTxInf createDrctDbtTxInf() {
        return new DrctDbtTxInf();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link CmpsdNmd }
     * 
     */
    public CmpsdNmd createCmpsdNmd() {
        return new CmpsdNmd();
    }

    /**
     * Create an instance of {@link CmpsdTpInf.SvcLvl }
     * 
     */
    public CmpsdTpInf.SvcLvl createCmpsdTpInfSvcLvl() {
        return new CmpsdTpInf.SvcLvl();
    }

    /**
     * Create an instance of {@link CmpsdTpInf.LclInstrm }
     * 
     */
    public CmpsdTpInf.LclInstrm createCmpsdTpInfLclInstrm() {
        return new CmpsdTpInf.LclInstrm();
    }

    /**
     * Create an instance of {@link CmpsdIdt.Id.PrvtId.Othr.SchmeNm }
     * 
     */
    public CmpsdIdt.Id.PrvtId.Othr.SchmeNm createCmpsdIdtIdPrvtIdOthrSchmeNm() {
        return new CmpsdIdt.Id.PrvtId.Othr.SchmeNm();
    }

    /**
     * Create an instance of {@link CmpsdAgt.FinInstnId.Othr }
     * 
     */
    public CmpsdAgt.FinInstnId.Othr createCmpsdAgtFinInstnIdOthr() {
        return new CmpsdAgt.FinInstnId.Othr();
    }

    /**
     * Create an instance of {@link CmpsdTx.MndtRltdInf }
     * 
     */
    public CmpsdTx.MndtRltdInf createCmpsdTxMndtRltdInf() {
        return new CmpsdTx.MndtRltdInf();
    }

}
