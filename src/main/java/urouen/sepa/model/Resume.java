package urouen.sepa.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import urouen.sepa.model.Resume.TrxResume;

public class Resume extends ArrayList<TrxResume>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6787665991466824906L;
	
	public Resume() {
		super();
	}
	
		
	public static class TrxResume {
		
		protected String num;
		
		protected String pmtId;
		
		protected double amount;
		
		protected String date;
	
		public TrxResume(String num, String pmtId, double amount, Calendar date) {
			super();
			this.num = num;
			this.pmtId = pmtId;
			this.amount = amount;
			setDate(date);
		}
		
		public TrxResume() {}
	
		public String getNum() {
			return num;
		}
	
		public void setNum(String num) {
			this.num = num;
		}
	
		public String getPmtId() {
			return pmtId;
		}
	
		public void setPmtId(String pmtId) {
			this.pmtId = pmtId;
		}
	
		public double getAmount() {
			return amount;
		}
	
		public void setAmount(double amount) {
			this.amount = amount;
		}
	
		public String getDate() {
			return date;
		}
	
		public void setDate(Calendar date) {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			this.date = format.format(date.getTime());
		}
	}
}
