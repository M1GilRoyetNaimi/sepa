package urouen.sepa.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import urouen.sepa.model.Amount;
import urouen.sepa.model.CmpsdAgt;
import urouen.sepa.model.CmpsdIdt;
import urouen.sepa.model.CmpsdNmd;
import urouen.sepa.model.CmpsdTpInf;
import urouen.sepa.model.CmpsdTx;
import urouen.sepa.model.DrctDbtTxInf;
import urouen.sepa.repository.DrctDbtTxinfRepository;
import urouen.sepa.utils.ApplicationContextUtils;
import urouen.sepa.utils.DrctDbtTxInfXMLConverter;


@EnableWebMvc
@ComponentScan(basePackages = {"urouen.sepa.controller"})
@SpringBootApplication
@EnableJpaRepositories(basePackageClasses=DrctDbtTxinfRepository.class)
@EntityScan(basePackageClasses=DrctDbtTxInf.class)
public class AppConfig {
	
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class);
	}
	
	@Value(value = "classpath:drctDbtTxInf.xsd")
	private Resource drctDbtTxInfSchema;
	
	
	
	@Bean
	public DrctDbtTxInfXMLConverter drctDbtTxInfXMLConverter(MarshallingHttpMessageConverter converter) {
		return new DrctDbtTxInfXMLConverter(converter);
	}
	
	
	@Bean
	public MarshallingHttpMessageConverter marshallingHttpMessageConverter(Jaxb2Marshaller marshaller) {
		MarshallingHttpMessageConverter converter = new MarshallingHttpMessageConverter();
		
		
		converter.setMarshaller(marshaller);
		converter.setUnmarshaller(marshaller);
		return converter;
	}
	
	@Bean
	public ApplicationContextUtils getApplicationContextUtils(ApplicationContext appContext) {
		ApplicationContextUtils ctxUtil = new ApplicationContextUtils();
		ctxUtil.setApplicationContext(appContext);
		return ctxUtil;
	}

	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setSchema(drctDbtTxInfSchema);
		marshaller.setClassesToBeBound(DrctDbtTxInf.class,Amount.class, CmpsdAgt.class,
				CmpsdIdt.class, CmpsdNmd.class, CmpsdTpInf.class, CmpsdTx.class);
		
		return marshaller;
	}
	
	@Bean
	public MappingJackson2XmlHttpMessageConverter xmlConverter() {
		MappingJackson2XmlHttpMessageConverter jackson =  new MappingJackson2XmlHttpMessageConverter();
		XmlMapper mapper = new XmlMapper();
		mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
		jackson.setObjectMapper(mapper);
		return jackson;
	}
	
	
	@Bean
	public StringHttpMessageConverter stringConverter() {
		return new StringHttpMessageConverter();
	}
	
	@Configuration
	@EnableWebMvc
	public class RestConfig extends WebMvcConfigurerAdapter {

		@Autowired
		private DrctDbtTxInfXMLConverter trxConverter;
		
		@Autowired
		private MappingJackson2XmlHttpMessageConverter xmlConverter;
		
		@Autowired
		private StringHttpMessageConverter stringConverter;
		
	    @Override
	    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

	        converters.add(trxConverter);
	        converters.add(xmlConverter);
	        converters.add(stringConverter);
	        super.configureMessageConverters(converters);
	    }
	}
	
}

