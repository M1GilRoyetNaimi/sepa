package urouen.sepa.repository;

import org.springframework.data.repository.CrudRepository;

import urouen.sepa.model.DrctDbtTxInf;


public interface DrctDbtTxinfRepository extends CrudRepository<DrctDbtTxInf, Long> {
	
	DrctDbtTxInf findByPmtId(String id);
	
}
