package urouen.sepa.controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HomeController {

	@RequestMapping(value="/", method = RequestMethod.GET)
	public @ResponseBody String home() {
		return "<h3>Naimi Guillaume, Royet Etienne</h3>"
				+ "<table>"
					+ "<tr>"
						+ "<th>URL</th><th>Méthode</th><th>Action</th>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>/resume</td><td>GET</td>"
						+ "<td>Affiche sous forme résumée, la liste des transactions enregistrées.</td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>/stats</td><td>GET</td>"
						+ "<td>Affiche une synthèse des transactions stockées, avec les informations suivantes : Nombre de transactions, montant total des transactions.</td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>/trx/n</td><td>GET</td>"
						+ "<td>renvoie un flux XML décrivant le détail de la transactions d'identifiant Id avec Id = PmtId.</td>"
					+ "</tr>"
					+ "<tr>"
						+ "<td>/depot</td><td>POST</td>"
						+ "<td>Dépose une nouvelle transaction. Une vérification syntaxique du contenu est <br>"
						+ "effectuée, afin de n'autoriser que des dépôts corrects (validation XSD par leservice). <br>"
						+ "Un message de retour indique le résultat de l'opération, avec le numéro d'identification en cas de succès,<br>"
						+ " et un message d'erreur sinon. <br>"
						+ "Remarque : En cas de doublon (même identifiant) la transaction transmise ne sera"
						+ " pas enregistrée.</td>"
					+ "</tr>"
				+ "</table>";
	}
	
}