package urouen.sepa.controller;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import urouen.sepa.exception.ConflictException;
import urouen.sepa.exception.ResourceNotFoundException;
import urouen.sepa.model.Amount;
import urouen.sepa.model.CmpsdAgt;
import urouen.sepa.model.CmpsdAgt.FinInstnId;
import urouen.sepa.model.CmpsdIdt;
import urouen.sepa.model.CmpsdIdt.Id;
import urouen.sepa.model.CmpsdNmd;
import urouen.sepa.model.CmpsdTx;
import urouen.sepa.model.CmpsdTx.MndtRltdInf;
import urouen.sepa.model.DrctDbtTxInf;
import urouen.sepa.model.ObjectFactory;
import urouen.sepa.model.Resume;
import urouen.sepa.model.Resume.TrxResume;
import urouen.sepa.model.Stats;
import urouen.sepa.repository.DrctDbtTxinfRepository;

@RestController
@RequestMapping("/")
public class SepaController {
	
	@Autowired
	private DrctDbtTxinfRepository drctDbtTxinfRepository;
	
	
	
	
	@RequestMapping(value ="/resume", method = RequestMethod.GET)
	public Resume getResume() {
		Resume result = new Resume();
		
		Iterable<DrctDbtTxInf> transactions = drctDbtTxinfRepository.findAll();
		
		transactions.forEach(transaction -> {
			result.add(new TrxResume(transaction.getNum(), 
							transaction.getPmtId(), 
							transaction.getInstdAmt().getValue().doubleValue(),
							transaction.getDrctDbtTx().getMndtRltdInf()
								.getDtOfSgntr()
								.toGregorianCalendar()));
		});
		
		return result;
 	}
	
	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	public ResponseEntity<Stats> getStats() {
		
		Iterable<DrctDbtTxInf> transactions = drctDbtTxinfRepository.findAll();
		
		int count = 0;
		double total = 0;
		 
		for (DrctDbtTxInf transac : transactions) {
			count++;
			total += transac.getInstdAmt().getValue().doubleValue();
		}
		
		Stats stats = new Stats(count, total);
		
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(stats);
	}
	
	 
	@RequestMapping(value = "/trx/{id}", method = RequestMethod.GET)
	public ResponseEntity<DrctDbtTxInf> getTransaction(@PathVariable String id) {
		
		
		DrctDbtTxInf result = drctDbtTxinfRepository.findByPmtId(id);
				
		if (result == null) {
			throw new ResourceNotFoundException();
		}
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(result);
	}
	
	
	
	@RequestMapping(value = "/depot", method = RequestMethod.POST, produces = {MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<String> insert(@RequestBody DrctDbtTxInf drctDbtTxInf) {
		
		if (drctDbtTxinfRepository.findByPmtId(drctDbtTxInf.getPmtId()) != null) {
			throw new ConflictException();
		}
		
		drctDbtTxinfRepository.save(drctDbtTxInf);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(drctDbtTxInf.getNum());
	}
	
	@RequestMapping(value ="/sample", method = RequestMethod.GET)
	public DrctDbtTxInf getSample() {
		return createTransactionSample();
	}
	
	private DrctDbtTxInf createTransactionSample() {
		ObjectFactory factory = new ObjectFactory();
		DrctDbtTxInf sepa = factory.createDrctDbtTxInf();
		
		sepa.setPmtId("REF OPE BBBB");
		Amount amount = factory.createAmount();
		amount.setCcy("EUR");
		amount.setValue(new BigDecimal(2150.08));
		sepa.setInstdAmt(amount);
		
		CmpsdTx cmpsdTx = factory.createCmpsdTx();
		
		MndtRltdInf mndtRltdInf = factory.createCmpsdTxMndtRltdInf();
		mndtRltdInf.setMndtId("MANDAT NO 666666");
		GregorianCalendar dateG = new GregorianCalendar();
		XMLGregorianCalendar date = null;
		try {
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateG );
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		mndtRltdInf.setDtOfSgntr(date);
		cmpsdTx.setMndtRltdInf(mndtRltdInf);
		sepa.setDrctDbtTx(cmpsdTx);
		
		
		CmpsdAgt cmpsdAgt = factory.createCmpsdAgt();
		FinInstnId finInstnId = factory.createCmpsdAgtFinInstnId();
		finInstnId.setBIC("BANKGBUL");
		cmpsdAgt.setFinInstnId(finInstnId);
		sepa.setDbtrAgt(cmpsdAgt);
		
		CmpsdNmd cmpsdNmd = factory.createCmpsdNmd();
		cmpsdNmd.setNm("Mr Debiteur N2");
		sepa.setDbtr(cmpsdNmd);
		
		CmpsdIdt cmpsdIdt = factory.createCmpsdIdt();
		Id idCmpsdIdt = factory.createCmpsdIdtId();
		idCmpsdIdt.setIBAN("GB29NWBK60161331926819");
		cmpsdIdt.setId(idCmpsdIdt);
		sepa.setDbtrAcct(cmpsdIdt);
		
		sepa.getRmtInf().add("Facture reference ISO 654321");
		
		return sepa;
	
	}


}
